## Barcelona

----

Texta amb informació de barcelona, bonica ciutat

#### Llista ordenada de barris
1. Noubarris
2. Guinardo
3. Trinitat

#### Llista desordenada de barris

- Trinitat Nova
- Guineueta
- Ciutat vella

#### Enllaç a una web [itb](https://www.itb.cat/)

### Barcelona

----

![barça](bcn.jpeg)